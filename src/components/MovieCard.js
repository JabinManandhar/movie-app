import React from "react";

export default function MovieCard({ movie }) {
  const title =
    movie.title.substring(0, 25) + (movie.title.length > 25 ? "..." : "");

  return (
    <div className="card">
      <div>
        <img
          src={`https://image.tmdb.org/t/p/w185_and_h278_bestv2/${movie.poster_path}`}
          alt={movie.title + " poster image"}
        />
      </div>
      <a href={`https://www.themoviedb.org/movie/${movie.id}-${movie.title}`}>
        {title}
      </a>
      <p>{movie.release_date}</p>
    </div>
  );
}
